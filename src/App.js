import React, { useState } from 'react'
import Form from './Form'
import './App.css'
import DataTable from './DataTable'

const App = () => {


  /**
   * INITIAL VALUES FOR FORM
   */

  const [initialValues, setInitialValues] = useState({
    id: "",
    name: "",
    email: "",
    contact: ""
  })


  /***
   * DATA FOR MAIN TABLE
   */
  const [mainTableData, setMainTableData] = useState([])


  /***
   * DATA FOR TRASH TABLE
   */
  const [trashTableData, setTrashTableData] = useState([])


  /***
   * SUBMIT METHOD FOR FORM
   */
  const handleContactFormSubmit = (data, resetForm) => {
    /***
     * IF DATA FROM FORM HAS ID THEN UPDATE DATA ELSE POST DATA
     */
    if (data.id) {
      const findIndex = mainTableData.findIndex(item => item.id === data.id)
      if (findIndex !== -1) {
        let newData = [...mainTableData]
        newData[findIndex] = data

        setMainTableData(newData)
      }
      setInitialValues({
        id: "",
        name: "",
        email: "",
        contact: ""
      })
    } else {
      setMainTableData([...mainTableData, { ...data, id: Math.random() }])
      resetForm()
    }
  }


  /***
   * TO REMOVE CHECKED ITEMS FROM MAIN TABLE AND ADD TO TRASH TABLE
   */

  const handleRemoveItemsFromMainTable = ids => {
    const newMainData = mainTableData.filter(item => !ids.includes(item.id))
    const newTrashData = mainTableData.filter(item => ids.includes(item.id))
    setMainTableData(newMainData)
    setTrashTableData([...trashTableData, ...newTrashData])
  }


  /***
   * TO RESTORE ITEMS FROM TRASH TABLE TO MAIN TABLE
   */
  const handleRestoreItemsFromTrashTable = ids => {
    const newMainData = trashTableData.filter(item => ids.includes(item.id))
    const newTrashData = trashTableData.filter(item => !ids.includes(item.id))

    setTrashTableData(newTrashData)
    setMainTableData([...mainTableData, ...newMainData])
  }


  /***
   * HANDLE REFILL FORM WHEN CLICKED ON EDIT
   */

  const handleEditButtonClicked = id => {

    const dataFind = mainTableData.find(item => item.id === id)
    if (dataFind) {
      setInitialValues({ ...dataFind })
    }
  }


  return (
    <div className="mx-5">
      <div className="row ">
        <div className="col-12 mb-2">
          <h4>React Assignment - I </h4>
        </div>
        <div className="col-12 mb-5">
          <Form
            initialValues={initialValues}
            handleContactFormSubmit={handleContactFormSubmit}
          />
        </div>
        <div className="col-12 mb-4">
          <DataTable
            title="Data Table"
            type="success"
            tableData={mainTableData}
            handleRemoveItemsFromMainTable={handleRemoveItemsFromMainTable}
            handleEditButtonClicked={handleEditButtonClicked}
            deletable
            editable
          />
        </div>
        <div className="col-12 mb-4">
          <DataTable
            title="Trash Table"
            type="danger"
            restorable
            tableData={trashTableData}
            handleRestoreItemsFromTrashTable={handleRestoreItemsFromTrashTable}
            handleEditButtonClicked={handleEditButtonClicked}
          />
        </div>
      </div>
    </div>
  )
}

export default App
