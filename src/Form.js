import React from 'react'
import { useFormik } from 'formik'
import * as Yup from 'yup';

const Form = ({ initialValues, handleContactFormSubmit }) => {
    const { handleSubmit, handleChange, handleBlur, values, touched, errors } = useFormik({
        initialValues,
        enableReinitialize: true,
        validationSchema: Yup.object({
            name: Yup.string().required("Name is required."),
            email: Yup.string().email().required("Email is required."),
            contact: Yup.number().required("Contact is required."),
        }),
        onSubmit: (values, { resetForm }) => {
            handleContactFormSubmit(values, resetForm)
        }

    })

    return (
        <div>
            <h5 className="font-weight-bold">Contact Form</h5>
            <form onSubmit={handleSubmit}>
                <div className="row align-items-center">
                    <div className="col-3">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Name"
                            name="name"
                            value={values.name}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                        {
                            errors.name && touched.name &&
                            <small className="text-danger">{errors.name}</small>
                        }
                    </div>
                    <div className="col-4">
                        <input
                            type="email"
                            className="form-control"
                            placeholder="Email"
                            name="email"
                            value={values.email}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                        {
                            errors.email && touched.email &&
                            <small className="text-danger">{errors.email}</small>
                        }
                    </div>
                    <div className="col-3">
                        <input
                            type="number"
                            className="form-control"
                            placeholder="Contact"
                            name="contact"
                            value={values.contact}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                        {
                            errors.contact && touched.contact &&
                            <small className="text-danger">{errors.contact}</small>
                        }
                    </div>
                    <div className="col-2">
                        <button type="submit" className="btn btn-sm btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default Form
