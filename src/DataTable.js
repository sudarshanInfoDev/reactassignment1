import React, { useState } from 'react'
import { FiEdit } from 'react-icons/fi'
import { RiDeleteBin5Line } from 'react-icons/ri'
import { MdSettingsBackupRestore } from 'react-icons/md'

const DataTable = ({
    title,
    type,
    tableData,
    editable,
    restorable,
    deletable,
    handleRemoveItemsFromMainTable,
    handleRestoreItemsFromTrashTable,
    handleEditButtonClicked
}) => {

    const [selectedId, setSelectedId] = useState([])

    const handleSelectSingleCheckbox = (id) => {
        /***
         * IF id IS ALREADY PRESENT IN ARRAY THEN REMOVE IT ELSE INSERT IN ARRAY
         */
        if (selectedId.includes(id)) {
            setSelectedId(selectedId.filter(item => item !== id))
        } else {
            setSelectedId([...selectedId, id])
        }
    }

    const handleSelectTopCheckbox = (e) => {
        /***
         * SET CHECKED VALUE ONLY ID TOP CHECKBOX IS CHECKED
         */
        if (e.target.checked) {
            if (tableData) {
                setSelectedId(tableData.map(item => item.id))
            } else {
                setSelectedId([])
            }
        } else {
            setSelectedId([])
        }
    }

    return (
        <div className="row">
            <div className="col-10"> <h5 className="font-weight-bold">{title}</h5></div>
            <div className="col-2 text-right">
                {
                    editable &&
                    <button
                        className={`btn btn-outline-primary btn-sm ${selectedId.length !== 1 ? "cursor-blocked" : ""}`}
                        onClick={() => { handleEditButtonClicked(selectedId[0]); setSelectedId([]) }}
                        disabled={selectedId.length !== 1}
                    >
                        <FiEdit />&nbsp;
                        Edit
                    </button>
                }
            &nbsp;&nbsp;&nbsp;
            {
                    deletable &&
                    <button
                        className={`btn btn-outline-danger btn-sm ${selectedId.length < 1 ? "cursor-blocked" : ""}`}
                        onClick={() => { handleRemoveItemsFromMainTable(selectedId); setSelectedId([]) }}
                        disabled={selectedId.length < 1}
                    >
                        <RiDeleteBin5Line />&nbsp;
                        Delete
                    </button>
                }
                {
                    restorable &&
                    <button
                        className={`btn btn-outline-success btn-sm ${selectedId.length < 1 ? "cursor-blocked" : ""}`}
                        onClick={() => { handleRestoreItemsFromTrashTable(selectedId); setSelectedId([]) }}
                        disabled={selectedId.length < 1}
                    >
                        <MdSettingsBackupRestore />&nbsp;
                        Restore
                    </button>
                }
            </div>
            <div className="col-12">
                <div className="card">
                    <table className={`table table-striped table-${type || ""}`}>
                        <thead>
                            <tr>
                                <td>
                                    <div className="form-check">
                                        <input
                                            className="form-check-input"
                                            type="checkbox"
                                            id="disabledFieldsetCheck"
                                            checked={selectedId.length === tableData?.length}
                                            onChange={handleSelectTopCheckbox}
                                            disabled={!tableData?.length}
                                        />

                                    </div>
                                </td>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Contact</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                tableData && tableData.length
                                    ? tableData?.map(data => (
                                        <tr key={data.id}>
                                            <td>
                                                <div className="form-check">
                                                    <input
                                                        className="form-check-input"
                                                        type="checkbox"
                                                        checked={selectedId.includes(data.id)}
                                                        onChange={() => handleSelectSingleCheckbox(data.id)}
                                                    />

                                                </div>
                                            </td>
                                            <td>{data.name}</td>
                                            <td>{data.email}</td>
                                            <td>{data.contact}</td>
                                        </tr>
                                    ))
                                    : <tr className="text-center">
                                        <td colSpan="4" className="font-weight-bold">No Data Available</td>
                                    </tr>
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default DataTable
